
import logging
import sys
import json

logger = logging.getLogger(__name__)


def get_slugs_from_file():
    slugs = []
    with open("./slug.json") as json_file:
        slugs = json.load(json_file)
    return slugs


def is_invalid_res(res, slug):
    if res.status_code != 200:
        logger.warning(f"request failed {slug} ")
        return True
    try:
        res.json()
        return False
    except Exception as e:
        logger.warning(f"mechanic exception {slug} {e}")

    return True


def print_loading(index, total, slug):
    per_percent = int((index-1)*100/total)
    cur_precent = int(index*100/total)

    if cur_precent > per_percent or index == 0:
        sys.stdout.write(f"\r{cur_precent}% {slug}  ")
        sys.stdout.flush()
