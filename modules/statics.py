
mechanic_post_url = "https://mechanic.divar.ir/v1/posts/{}"
cardetails_post_url = "https://api.divar.ir/v8/cardetails/posts/{}"

api_keys = [
    "description",
    "images",
    "videos",
    "links",
    "searches",
    "similar_brands",
    "title",
    "en_title",
    "slug",
    "price_chart",
    "models",
    "parents",
    "options",
    "feedback"
]

special_api_keys = [
    "models",
    "images",
    "videos",
    "parents",
    "options"
]
