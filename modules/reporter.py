
import os
import json
from .statics import api_keys


def create_folder(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def export_json(report, file_name):
    create_folder("./reports")
    with open(file_name, "w", encoding="utf8") as report_file:
        data = json.dumps(report, indent=4, ensure_ascii=False)
        report_file.write(data)


class Reporter():

    missing_keys = {"report_title": "cardetails missing keys"}
    missing_keys_slug = {"report_title": "cardetails missing keys with details"}
    not_equal_keys = {"report_title": "not equal value"}
    not_equal_keys_details = {}
    failure_request_slug = []

    def __init__(self):
        for key in api_keys:
            Reporter.missing_keys[key] = 0
            Reporter.missing_keys_slug[key] = []
            Reporter.not_equal_keys[key] = 0
            Reporter.not_equal_keys_details[key] = []

    def report_missing_key(self, slug, key):
        Reporter.missing_keys[key] += 1
        Reporter.missing_keys_slug[key].append(slug)

    def report_not_equal(self, slug, key, m_value, c_value):
        Reporter.not_equal_keys[key] += 1
        details = {
            "slug": slug,
            "cardetails": c_value,
            "mechanic": m_value
        }
        Reporter.not_equal_keys_details[key].append(details)

    def report_failure(self, slug):
        Reporter.failure_request_slug.append(slug)

    def export(self):
        export_json(Reporter.missing_keys,
                    "reports/missing_keys.json")
        export_json(Reporter.missing_keys_slug,
                    "reports/missing_keys_slug.json")
        export_json(Reporter.not_equal_keys,
                    "reports/not_equal_keys.json")
        export_json(Reporter.failure_request_slug,
                    "reports/failure_request_slug.json")
        for key,report in Reporter.not_equal_keys_details.items():    
            export_json(report,
                    f"reports/differences_inـ{key}.json")
