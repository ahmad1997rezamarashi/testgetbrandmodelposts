
from .reporter import Reporter
from .statics import api_keys, special_api_keys


class CheckPostAPI():

    def __init__(self, mechanic_res, cardetails_res, reporter, slug):
        self._mechanic_res = mechanic_res.json()
        self._cardetails_res = cardetails_res.json()
        self._reporter = reporter
        self._slug = slug

    def _check_key(self, api_key):
        if api_key not in self._cardetails_res:
            self._reporter.report_missing_key(slug=self._slug,key=api_key)
            return None

        m_value = self._mechanic_res.get(api_key)
        c_value = self._cardetails_res.get(api_key)

        if not self._is_equal(m_value, c_value, api_key):
            self._reporter.report_not_equal(
                slug=self._slug, key=api_key,
                m_value=m_value, c_value=c_value
            )

    def test(self):
        for key in api_keys:
            self._check_key(key)

    def _is_equal(self, m_value, c_value, api_key):
        if api_key not in special_api_keys:
            return m_value == c_value

        if api_key == "models":
            is_equal_title = m_value.get("title") == c_value.get("title")
            is_equal_data = len(m_value["data"]) == len(c_value["data"])
            return is_equal_title and is_equal_data

        if api_key in special_api_keys:
            return len(m_value) == len(c_value)

        return False
