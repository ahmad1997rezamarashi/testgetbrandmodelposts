
import json
import requests
from modules.statics import api_keys, mechanic_post_url, cardetails_post_url
from modules.reporter import Reporter
from modules.check_post_api import CheckPostAPI
from utils import print_loading,get_slugs_from_file,is_invalid_res

def main():
    slugs = get_slugs_from_file()
    reporter = Reporter()

    for i, slug in enumerate(slugs):
        print_loading(index=i, total=len(slugs), slug=slug)

        mechanic_res = requests.get(mechanic_post_url.format(slug))
        cardetails_res = requests.get(cardetails_post_url.format(slug))

        if is_invalid_res(mechanic_res, slug) or is_invalid_res(mechanic_res, slug):
            reporter.report_failure(slug)
            continue

        check_api = CheckPostAPI(
            mechanic_res=mechanic_res,
            cardetails_res=cardetails_res,
            reporter=reporter,
            slug=slug
        )

        check_api.test()

    reporter.export()


if __name__ == '__main__':
    main()


